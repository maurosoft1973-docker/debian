### Variable
* `DOCKER_HUB_USER`: user docker hub
* `DOCKER_HUB_PASSWORD`: password docker hub
* `DOCKER_HOST`: "tcp://XXX.XXX.XXX.XXX:2375"

#Build Image Test with gitlab-runner
```bash
gitlab-runner exec docker --env DOCKER_HUB_USER="$DOCKER_HUB_USER" --env DOCKER_HUB_PASSWORD="$DOCKER_HUB_PASSWORD" --env DOCKER_HOST="$DOCKER_HOST" "amd64 test build"
```

#Build Image Current with gitlab-runner
```bash
gitlab-runner exec docker --env DOCKER_HUB_USER="$DOCKER_HUB_USER" --env DOCKER_HUB_PASSWORD="$DOCKER_HUB_PASSWORD" --env DOCKER_HOST="$DOCKER_HOST" "amd64 current build"
```

#Build Image Latest with gitlab-runner
```bash
gitlab-runner exec docker --env DOCKER_HUB_USER="$DOCKER_HUB_USER" --env DOCKER_HUB_PASSWORD="$DOCKER_HUB_PASSWORD" --env DOCKER_HOST="$DOCKER_HOST" "amd64 latest build"
```

#Update README with gitlab-runner
```bash
gitlab-runner exec docker --env DOCKER_HUB_USER="$DOCKER_HUB_USER" --env DOCKER_HUB_PASSWORD="$DOCKER_HUB_PASSWORD" --env DOCKER_HOST="$DOCKER_HOST" "update readme"
```
