#!/bin/bash
# Description: Build image and push to repository
# Maintainer: Mauro Cardillo
# DOCKER_HUB_USER and DOCKER_HUB_PASSWORD is user environment variable
echo "Get Remote Environment Variable from .env"
wget -q "https://gitlab.com/maurosoft1973-docker/alpine-variable/-/raw/master/.env" -O ./.env
source ./.env

BUILD_DATE=$(date +"%Y-%m-%d")
IMAGE=maurosoft1973/debian

 Loop through arguments and process them
for arg in "$@"
do
    case $arg in
        -ar=*|--debian-release=*)
        DEBIAN_RELEASE="${arg#*=}"
        shift # Remove
        ;;
        -av=*|--debian-version=*)
        DEBIAN_VERSION="${arg#*=}"
        shift # Remove
        ;;
        -avd=*|--debian-version-date=*)
        DEBIAN_VERSION_DATE="${arg#*=}"
        shift # Remove
        ;;
        -r=*|--release=*)
        RELEASE="${arg#*=}"
        shift # Remove
        ;;
        -h|--help)
        echo -e "usage "
        echo -e "$0 "
        echo -e "  -ar=|--debian-release -> ${DEBIAN_RELEASE} (debian release)"
        echo -e "  -av=|--debian-version -> ${DEBIAN_VERSION} (debian version)"
        echo -e "  -avd=|--debian-version-date -> ${DEBIAN_VERSION_DATE} (debian version date)"
        echo -e "  -r=|--release -> ${RELEASE} (release of image.Values: TEST, CURRENT, LATEST)"
        exit 0
        ;;
    esac
done

echo "# Image               : ${IMAGE}"
echo "# Image Release       : ${RELEASE}"
echo "# Build Date          : ${BUILD_DATE}"
echo "# Debian Release      : ${DEBIAN_RELEASE}"
echo "# Debian Version      : ${DEBIAN_VERSION}"
echo "# Debian Version Date : ${DEBIAN_VERSION_DATE}"

if [ ! -d "${DEBIAN_RELEASE}-${DEBIAN_VERSION}/amd64/" ]; then
    echo "The source of debian  ${DEBIAN_RELEASE}-${DEBIAN_VERSION} not exist"
    exit 1
fi

if [ "$RELEASE" == "TEST" ]; then
    echo "Remove image ${IMAGE}:${DEBIAN_RELEASE}-test"
    docker rmi -f ${IMAGE}:${DEBIAN_RELEASE}-test > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${DEBIAN_RELEASE}-test"
    docker rmi -f ${IMAGE}:${DEBIAN_RELEASE}-test > /dev/null 2>&1

    echo "Build Image: ${IMAGE} -> $RELEASE"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" -t ${IMAGE}:${DEBIAN_RELEASE}-test -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:${DEBIAN_RELEASE}-test"
    docker push ${IMAGE}:${DEBIAN_RELEASE}-test
elif [ "$RELEASE" == "CURRENT" ]; then
    echo "Remove image ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}"
    docker rmi -f ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION} > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-amd64"
    docker rmi -f ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-amd64 > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-x86_64"
    docker rmi -f ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-x86_64 > /dev/null 2>&1

    echo "Build Image: ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION} -> $RELEASE"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" -t ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION} -t ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-amd64 -t ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-x86_64 -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-amd64"
    docker push ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-amd64

    echo "Push Image -> ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-x86_64"
    docker push ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}-x86_64

    echo "Push Image -> ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}"
    docker push ${IMAGE}:${DEBIAN_RELEASE}-${DEBIAN_VERSION}
else
    echo "Remove image ${IMAGE}:latest"
    docker rmi -f ${IMAGE} > /dev/null 2>&1

    echo "Remove image ${IMAGE}:amd64"
    docker rmi -f ${IMAGE}:amd64 > /dev/null 2>&1

    echo "Remove image ${IMAGE}:x86_64"
    docker rmi -f ${IMAGE}:x86_64 > /dev/null 2>&1

    echo "Build Image: ${IMAGE} -> $RELEASE"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" -t ${IMAGE}:latest -t ${IMAGE}:amd64 -t ${IMAGE}:x86_64 -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:amd64"
    docker push ${IMAGE}:amd64

    echo "Push Image -> ${IMAGE}:x86_64"
    docker push ${IMAGE}:x86_64

    echo "Push Image -> ${IMAGE}:latest"
    docker push ${IMAGE}:latest
fi

rm -rf ./.env
