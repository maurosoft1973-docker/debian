FROM scratch

ARG BUILD_DATE
ARG DEBIAN_RELEASE
ARG DEBIAN_VERSION
ARG DEBIAN_VERSION_DATE

LABEL maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="amd64/x86_64" \
    debian-version="$DEBIAN_RELEASE-$DEBIAN_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="debian" \
    org.opencontainers.image.description="Docker image running on Debian Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$DEBIAN_VERSION" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/debian/" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/debian" \
    org.opencontainers.image.created=$BUILD_DATE

ADD $DEBIAN_RELEASE-$DEBIAN_VERSION/amd64/rootfs.tar.xz /

RUN export DEBIAN_FRONTEND="noninteractive" && \
    apt update && \
    apt upgrade -y && \
    apt install -y net-tools locales wget && \
    rm -rf /var/lib/apt/lists/* \ && \
    mkdir /scripts

COPY files /scripts/

RUN chmod -R 755 /scripts

ENTRYPOINT ["/scripts/run-debian.sh"]
