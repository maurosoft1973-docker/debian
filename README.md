# Debian Linux in Docker with Multilanguage e Timezone support

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/debian.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/debian/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/debian.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/debian/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/debian.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/debian/)

This Docker image [(maurosoft1973/debian)](https://hub.docker.com/r/maurosoft1973/debian/) is based on the debian distribution.

##### Debian Version 10.9 (Released Mar 27, 2021)

----

## Architectures

* ```:amd64```, ```:x86_64``` - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest``` latest branch based (Automatic Architecture Selection)
* ```:amd64```, ```:x86_64```  amd64 based on latest tag but amd64 architecture

## Layers & Sizes

![Version](https://img.shields.io/badge/version-amd64-blue.svg?style=for-the-badge)
![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/debian/latest?style=for-the-badge)

## Environment Variables:

### Main parameters:
* `LANG`: default locale (en_GB.UTF-8)
* `TIMEZONE`: default timezone (Europe/Brussels)

#### List of locale Sets

When setting locale, also make sure to choose a locale otherwise it will be the default (en_GB.UTF-8).

```
+-----------------+
| Locale          |
+-----------------+
| ch_DE.UTF-8     |
| fr_FR.UTF-8     |
| de_CH.UTF-8     |
| de_DE.UTF-8     |
| en_GB.UTF-8     |
| en_US.UTF-8     |
| es_ES.UTF-8     |
| it_IT.UTF-8     |
| nb_NO.UTF-8     | 
| nl_NL.UTF-8     |
| pt_BR.UTF-8     |
| ru_RU.UTF-8     |
| sv_SE.UTF-8     |
+-----------------+
```

## Creating an instance (default timezone and locale)

```bash
docker run -it --name debian maurosoft1973/debian
```

## Creating an instance with locale it_IT

```bash
docker run -it --name debian -e LANG=it_IT.UTF-8 maurosoft1973/debian
```
## Creating an instance with locale it_IT and timezone Europe/Rome

```bash
docker run -it --name debian -e LANG=it_IT.UTF-8 -e TIMEZONE=Europe/Rome maurosoft1973/debian
```

***
###### Last Update 19.04.2021 10:52:52
