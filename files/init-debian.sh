#!/bin/bash

INTERFACE=$(route | grep "default" | awk '{print $8}')
IP=$(ifconfig $INTERFACE | grep "inet " | cut -d: -f2 | awk '{print $2}')
LANG=${LANG:-"en_GB.UTF-8"}
TIMEZONE=${TIMEZONE:-"Europe/Brussels"}

# Check Locale
CHECK=$(grep ${LANG} /etc/locale.gen | wc -l)
if [ ${CHECK} == 1 ]; then
    echo "Locale current is ${LANG}"
    IFS='.' read -r LANGUAGE CTYPE <<< "${LANG}"
    localedef -i ${LANGUAGE} -c -f ${CTYPE} -A /usr/share/locale/locale.alias ${LANG}
else 
    echo "Locale ${LANG} not supported or not exist"
    echo "Locale availables:"
    cat /etc/locale.gen
    echo ""
    echo "Locale current is en_GB.UTF-8"
fi

# Check Timezone
CHECK==$(test -f /usr/share/zoneinfo/${TIMEZONE})
if [ $? == 0 ]; then
    echo "Timezone current is ${TIMEZONE}"
    cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
    export TIMEZONE
else 
    echo "Timezone ${TIMEZONE} not supported or not exist"
    echo "Timezone current is Europe/Brussels"
fi

# Network
echo "Interface ${INTERFACE}"
export INTERFACE

echo "IP ${IP}"
export IP
